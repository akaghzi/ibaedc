require "application_system_test_case"

class CoursesTest < ApplicationSystemTestCase
  setup do
    @course = courses(:one)
  end

  test "visiting the index" do
    visit courses_url
    assert_selector "h1", text: "Courses"
  end

  test "creating a Course" do
    visit courses_url
    click_on "New Course"

    fill_in "Advertised at", with: @course.advertised_at
    fill_in "Course title", with: @course.course_title_id
    fill_in "Duration", with: @course.duration
    fill_in "Fees", with: @course.fees
    fill_in "Finish date", with: @course.finish_date
    fill_in "Publish at", with: @course.publish_at
    fill_in "Start date", with: @course.start_date
    fill_in "User", with: @course.user_id
    click_on "Create Course"

    assert_text "Course was successfully created"
    click_on "Back"
  end

  test "updating a Course" do
    visit courses_url
    click_on "Edit", match: :first

    fill_in "Advertised at", with: @course.advertised_at
    fill_in "Course title", with: @course.course_title_id
    fill_in "Duration", with: @course.duration
    fill_in "Fees", with: @course.fees
    fill_in "Finish date", with: @course.finish_date
    fill_in "Publish at", with: @course.publish_at
    fill_in "Start date", with: @course.start_date
    fill_in "User", with: @course.user_id
    click_on "Update Course"

    assert_text "Course was successfully updated"
    click_on "Back"
  end

  test "destroying a Course" do
    visit courses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Course was successfully destroyed"
  end
end
