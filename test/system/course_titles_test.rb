require "application_system_test_case"

class CourseTitlesTest < ApplicationSystemTestCase
  setup do
    @course_title = course_titles(:one)
  end

  test "visiting the index" do
    visit course_titles_url
    assert_selector "h1", text: "Course Titles"
  end

  test "creating a Course title" do
    visit course_titles_url
    click_on "New Course Title"

    fill_in "Active", with: @course_title.active
    fill_in "Description", with: @course_title.description
    fill_in "Name", with: @course_title.name
    click_on "Create Course title"

    assert_text "Course title was successfully created"
    click_on "Back"
  end

  test "updating a Course title" do
    visit course_titles_url
    click_on "Edit", match: :first

    fill_in "Active", with: @course_title.active
    fill_in "Description", with: @course_title.description
    fill_in "Name", with: @course_title.name
    click_on "Update Course title"

    assert_text "Course title was successfully updated"
    click_on "Back"
  end

  test "destroying a Course title" do
    visit course_titles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Course title was successfully destroyed"
  end
end
