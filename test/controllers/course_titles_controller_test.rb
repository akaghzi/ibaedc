require 'test_helper'

class CourseTitlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @course_title = course_titles(:one)
  end

  test "should get index" do
    get course_titles_url
    assert_response :success
  end

  test "should get new" do
    get new_course_title_url
    assert_response :success
  end

  test "should create course_title" do
    assert_difference('CourseTitle.count') do
      post course_titles_url, params: { course_title: { active: @course_title.active, description: @course_title.description, name: @course_title.name } }
    end

    assert_redirected_to course_title_url(CourseTitle.last)
  end

  test "should show course_title" do
    get course_title_url(@course_title)
    assert_response :success
  end

  test "should get edit" do
    get edit_course_title_url(@course_title)
    assert_response :success
  end

  test "should update course_title" do
    patch course_title_url(@course_title), params: { course_title: { active: @course_title.active, description: @course_title.description, name: @course_title.name } }
    assert_redirected_to course_title_url(@course_title)
  end

  test "should destroy course_title" do
    assert_difference('CourseTitle.count', -1) do
      delete course_title_url(@course_title)
    end

    assert_redirected_to course_titles_url
  end
end
