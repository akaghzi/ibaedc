class Equipment < ApplicationRecord
  validates :name, presence: true
  validates :description, presence: true
  validates :asset_tag, presence: true, uniqueness: {case_sensitive: false}
end
