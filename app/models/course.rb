class Course < ApplicationRecord
  # include Elasticsearch::Model
  # include Elasticsearch::Model::Callbacks

  belongs_to :course_title
  validates :duration, presence: true
  validates :fees, numericality: true

  def as_indexed_json(options={})
    {
      "course_title_name": course_title.name,
      "course_title_description": course_title.description,
      "duration": duration,
      "fees": fees,
      "start_date": start_date,
      "finish_date": finish_date
    }
  end

end