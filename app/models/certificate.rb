class Certificate < ApplicationRecord
  validates :name, presence: true, uniqueness: {case_sensitive: false}
  validates :duration_in_years, numericality: {greater_than_or_equal_to: 0, less_than: 20}
end
