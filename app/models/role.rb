class Role < ApplicationRecord
  validates :name, :description, presence: true, uniqueness: {case_sensitive: false}
  before_save {|role|role.name = role.name.downcase}
end
