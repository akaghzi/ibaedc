class CourseTitle < ApplicationRecord

  # include Elasticsearch::Model
  # include Elasticsearch::Model::Callbacks

  has_many :courses
  validates :name, presence: true, uniqueness: {case_sensitive: false}
  validates :description, presence: true

  # after_save :index_courses_in_elasticsearch


  def as_indexed_json(options = {})
    self.as_json(
      only: [:id, :name, :description, :active]
    )
  end  

  private 
  def index_courses_in_elasticsearch
    courses.find_each { |course| course.__elasticsearch__.index_document }
  end

end
