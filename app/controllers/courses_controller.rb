class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  # GET /courses
  # GET /courses.json
  def index
    if params[:query]
      response = Course.__elasticsearch__.search(params[:query]).results
      render json: {
        results: response.results,
        total: response.total
      }
    else
      @courses = Course.all
    end
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
    if @course.added_by
      @adder = User.find(@course.added_by)
    end
    if @course.changed_by
      @changer = User.find(@course.changed_by)
    end
    if @course.archived_by
      @archiver = User.find(@course.archived_by)
    end
  end

  # GET /courses/new
  def new
    @course = Course.new
    @course_titles = CourseTitle.where(active: true)
  end

  # GET /courses/1/edit
  def edit
    @course_titles = CourseTitle.where(active: true)
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)
    @course.added_by = current_user.id
    @course.added_at = Time.now
    
    respond_to do |format|
      if @course.save
        format.html { redirect_to @course, notice: 'Course was successfully created.' }
        format.json { render :show, status: :created, location: @course }
      else
        format.html { render :new }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    @course.changed_by = current_user.id
    @course.changed_at = Time.now
    
    respond_to do |format|
      if @course.update(course_params)
        format.html { redirect_to @course, notice: 'Course was successfully updated.' }
        format.json { render :show, status: :ok, location: @course }
      else
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    # @course.destroy
    # respond_to do |format|
    #   format.html { redirect_to courses_url, notice: 'Course was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:course_title_id, :start_date, :finish_date, :duration, :fees, :publish_at, :advertised_at, :changed_by, :changed_at, :archived_by, :archived_at, :days_of_week, :time_slots)
    end
end
