class CourseTitlesController < ApplicationController
  before_action :set_course_title, only: [:show, :edit, :update, :destroy]

  # GET /course_titles
  # GET /course_titles.json
  def index
    if params[:query]
      response = CourseTitle.__elasticsearch__.search(params[:query]).results

      render json: {
        results: response.results,
        total: response.total
      }
    else
      @course_titles = CourseTitle.all
    end

  end

  # GET /course_titles/1
  # GET /course_titles/1.json
  def show
    if @course_title.added_by
      @adder = User.find(@course_title.added_by)
    end
    if @course_title.changed_by
      @changer = User.find(@course_title.changed_by)
    end
    if @course_title.archived_by
      @archiver = User.find(@course_title.archived_by)
    end
    
  end

  # GET /course_titles/new
  def new
    @course_title = CourseTitle.new
  end

  # GET /course_titles/1/edit
  def edit
  end

  # POST /course_titles
  # POST /course_titles.json
  def create
    @course_title = CourseTitle.new(course_title_params)
    @course_title.added_by = current_user.id
    @course_title.added_at = Time.now

    respond_to do |format|
      if @course_title.save
        format.html { redirect_to @course_title, notice: 'Course title was successfully created.' }
        format.json { render :show, status: :created, location: @course_title }
      else
        format.html { render :new }
        format.json { render json: @course_title.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /course_titles/1
  # PATCH/PUT /course_titles/1.json
  def update
    @course_title.changed_by = current_user.id
    @course_title.changed_at = Time.now

    respond_to do |format|
      if @course_title.update(course_title_params)
        format.html { redirect_to @course_title, notice: 'Course title was successfully updated.' }
        format.json { render :show, status: :ok, location: @course_title }
      else
        format.html { render :edit }
        format.json { render json: @course_title.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /course_titles/1
  # DELETE /course_titles/1.json
  def destroy
    # @course_title.destroy
    # respond_to do |format|
    #   format.html { redirect_to course_titles_url, notice: 'Course title was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course_title
      @course_title = CourseTitle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_title_params
      params.require(:course_title).permit(:user_id, :name, :description, :active, :changed_by, :changed_at, :archived_by, :archived_at, :eligibility, :outline)
    end
end
