json.extract! course, :id, :user_id, :course_title_id, :start_date, :finish_date, :duration, :fees, :publish_at, :advertised_at, :created_at, :updated_at
json.url course_url(course, format: :json)
