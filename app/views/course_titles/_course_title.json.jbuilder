json.extract! course_title, :id, :name, :description, :active, :created_at, :updated_at
json.url course_title_url(course_title, format: :json)
