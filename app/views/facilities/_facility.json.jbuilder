json.extract! facility, :id, :name, :description, :added_by, :added_at, :changed_by, :changed_at, :archived_by, :archived_at, :created_at, :updated_at
json.url facility_url(facility, format: :json)
