json.extract! transport, :id, :name, :description, :added_by, :added_at, :changed_by, :changed_at, :archived_by, :archived_at, :created_at, :updated_at
json.url transport_url(transport, format: :json)
