json.extract! certificate, :id, :name, :duration_in_years, :created_at, :updated_at
json.url certificate_url(certificate, format: :json)
