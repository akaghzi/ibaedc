class CreateCourseTitles < ActiveRecord::Migration[5.2]
  def change
    create_table :course_titles do |t|
      t.integer :added_by
      t.datetime :added_at
      t.integer :changed_by
      t.datetime :changed_at
      t.integer :archived_by
      t.datetime :archived_at
      t.string :name, null: false
      t.text :description, null: false
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
