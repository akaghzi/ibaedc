class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.references :course_title, foreign_key: true
      t.integer :added_by
      t.datetime :added_at
      t.integer :changed_by
      t.datetime :changed_at
      t.integer :archived_by
      t.datetime :archived_at
      t.date :start_date
      t.date :finish_date
      t.string :duration
      t.integer :fees
      t.date :publish_at
      t.date :advertised_at

      t.timestamps
    end
  end
end
