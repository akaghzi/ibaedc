class AddFieldsToCourseTitle < ActiveRecord::Migration[5.2]
  def change
    add_column :course_titles, :outline, :text
    add_column :course_titles, :eligibility, :string
  end
end
