class AddAssetTagToEquipment < ActiveRecord::Migration[5.2]
  def change
    add_column :equipment, :asset_tag, :string
  end
end
