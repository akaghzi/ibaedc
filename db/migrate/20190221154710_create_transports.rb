class CreateTransports < ActiveRecord::Migration[5.2]
  def change
    create_table :transports do |t|
      t.string :name
      t.string :description
      t.integer :added_by
      t.datetime :added_at
      t.integer :changed_by
      t.datetime :changed_at
      t.integer :archived_by
      t.datetime :archived_at

      t.timestamps
    end
  end
end
