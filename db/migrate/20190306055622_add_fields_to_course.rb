class AddFieldsToCourse < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :days_of_week, :string
    add_column :courses, :time_slots, :string
  end
end
