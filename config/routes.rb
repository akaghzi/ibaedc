Rails.application.routes.draw do
  resources :certificates
  resources :transports
  resources :facilities
  resources :equipment
  resources :roles
  resources :courses
  resources :course_titles
  devise_for :users, controllers: {
          sessions: 'users/sessions',
          registrations: 'users/registrations'
        }
  # devise_for :users
  root to: "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
